package main

import (
	"encoding/json"
	"fmt"
	"github.com/marni/goigc"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var PageSize int

//Init starts internal server clock
func Init() {
	Start()
	PageSize = 5
}

//ValidURL returns valid base urls"
func ValidURL() []string {
	return []string{"/igcinfo/api/", "/igcinfo/api/track/"}
}

//ValidURLSlice returns string slice of valid url
func ValidURLSlice() []string {
	return DeleteEmpty(strings.Split(ValidURL()[1], "/"))
}

//ValidField returns string slice of valid fields
func ValidField() []string {
	return []string{"h_date", "pilot", "glider", "glider_id", "track_length", "track_src_url"}
}

//APIInfo returns json for API info
func APIInfo(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	apiInfo := APIInfoFill()
	json.NewEncoder(w).Encode(apiInfo)
}

//APIInfoFill returns ApiInfo struct with rifht information
func APIInfoFill() (apiInfo APIInfoResponse) {
	apiInfo.Info = "Service for IGC tracks."
	apiInfo.Version = "v1"
	apiInfo.Duration = ISO8601()
	return
}

//URLIgcLen takes param URL, returns true when lenght of request is rights
func URLIgcLen(URL string) bool {
	if len(strings.Split(URL, "/")) == 5 {
		return true
	}
	return false
}

//RegisterIgcURL registers igc file url to internal memmory
func RegisterIgcURL(w http.ResponseWriter, r *http.Request) {
	var URL RegisterIgcStruct
	err := json.NewDecoder(r.Body).Decode(&URL)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	} else {
		_, err := igc.ParseLocation(URL.URL)
		if err == nil {
			db := SetUpDB()
			track, inDB := db.GetTrackByURL(URL.URL)
			var idResp IDResponse
			if inDB {
				idResp.ID = track.ID

			} else {
				var newTrack TrackInfo
				newTrack.ID = db.CountTracks()
				newTrack.URL = URL.URL
				newTrack.TimeStamp = time.Now().Unix()
				idResp.ID = newTrack.ID
				db.AddTrack(newTrack)
			}
			URLIDResp(w, idResp)
		} else {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		}
	}
}

//URLIDResp writes resp with ID to clients
func URLIDResp(w http.ResponseWriter, id IDResponse) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(id)
}

//GetTrackInfoF replies with slice of all map keys
func GetTrackInfo(w http.ResponseWriter) {
	var ids IDSliceResp
	db := SetUpDB()
	for i := 0; i < db.CountTracks(); i++ {
		ids.ID = append(ids.ID, i)
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ids)
}

//APICallWithID takes @param URL returns true when it is of right lenght and contains valid id field
func APICallWithID(URL string) bool {
	urlSlice := DeleteEmpty(strings.Split(URL, "/"))
	validURLSlice := ValidURLSlice()
	if len(urlSlice) == 4 && urlSlice[0] == validURLSlice[0] && urlSlice[1] == validURLSlice[1] && urlSlice[2] == validURLSlice[2] {
		_, err := strconv.Atoi(urlSlice[3])
		if err == nil {
			return true
		}
	}
	return false
}

//APICallWithField takes param URL returns true if ti contains right fields in request
func APICallWithField(URL string) bool {
	urlSlice := DeleteEmpty(strings.Split(URL, "/"))
	validURLSlice := ValidURLSlice()
	if len(urlSlice) == 5 && urlSlice[0] == validURLSlice[0] && urlSlice[1] == validURLSlice[1] && urlSlice[2] == validURLSlice[2] && contains(ValidField(), urlSlice[4]) {
		_, err := strconv.Atoi(urlSlice[3])
		if err == nil {
			return true
		}
	}
	return false
}

//GetGlideInfo replies with info about track from internal memmory.
func GetGlideInfo(w http.ResponseWriter, r *http.Request) {
	trackID, ok := strconv.Atoi(DeleteEmpty(strings.Split(r.URL.String(), "/"))[3])
	if ok == nil {
		db := SetUpDB()
		trackInfo, found := db.GetTrackByID(trackID)
		if found {

			track, err := igc.ParseLocation(trackInfo.URL)
			if err != nil {
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			} else {
				var info TrackInfoResp
				info.Date = track.Date.String()
				info.Pilot = track.Pilot
				info.Glider = track.GliderType
				info.GliderID = track.GliderID
				info.TrackLenght = track.Task.Distance()
				info.SourceURL = trackInfo.URL
				w.Header().Set("Content-Type", "application/json")
				json.NewEncoder(w).Encode(info)
			}
		}
	} else {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
	}
}

//GetFieldInfo replies with information from specified field
func GetFieldInfo(w http.ResponseWriter, r *http.Request) {
	trackID, err := strconv.Atoi(DeleteEmpty(strings.Split(r.URL.String(), "/"))[3])
	db := SetUpDB()
	if err == nil {
		trackInfo, found := db.GetTrackByID(trackID)
		if found {
			track, _ := igc.ParseLocation(trackInfo.URL)
			switch field := DeleteEmpty(strings.Split(r.URL.String(), "/"))[4]; field {
			case ValidField()[0]:
				fmt.Fprintln(w, track.Date.String())
			case ValidField()[1]:
				fmt.Fprintln(w, track.Pilot)
			case ValidField()[2]:
				fmt.Fprintln(w, track.GliderType)
			case ValidField()[3]:
				fmt.Fprintln(w, track.GliderID)
			case ValidField()[4]:
				fmt.Fprintln(w, strconv.FormatFloat(track.Task.Distance(), 'E', -1, 64))
			case ValidField()[5]:
				fmt.Fprintln(w, trackInfo.URL)
			default:
				http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
			}
		} else {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		}
	} else {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
	}
}

func TickerLatest(w http.ResponseWriter, r *http.Request) {
	db := SetUpDB()
	latestID := (db.CountTracks() - 1)
	trackInfo, found := db.GetTrackByID(latestID)
	if found {
		fmt.Fprintln(w, strconv.FormatInt(trackInfo.TimeStamp, 10))
	} else {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}
}

func Ticker(w http.ResponseWriter) {
	startTime := time.Now()
	var ticker TickerResp
	db := SetUpDB()
	tracks := db.CountTracks()
	var stopTrack TrackInfo
	if tracks >= PageSize {
		ticker.Tracks = getTracksForTicker(PageSize, 0)
		stopTrack, _ = db.GetTrackByID(PageSize - 1)
	} else {
		ticker.Tracks = getTracksForTicker(tracks, 0)
		stopTrack, _ = db.GetTrackByID(tracks - 1)
	}
	startTrack, _ := db.GetTrackByID(0)
	latestTrack, _ := db.GetTrackByID(tracks - 1)
	ticker.Start = startTrack.TimeStamp
	ticker.Stop = stopTrack.TimeStamp
	ticker.Latest = latestTrack.TimeStamp
	//Duration has no Milliseconds function
	ticker.Processing = time.Since(startTime).Nanoseconds() / 1000000

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ticker)

}

func getTracksForTicker(amount int, start int) (ids []int) {
	for i := start; i < start+amount; i++ {
		ids = append(ids, i)
	}
	return
}

//hepler_function
//Borrowed from http://dabase.com/e/15006/ Removes empty strings in slices
func DeleteEmpty(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}

//helper_function
//contains takes param s and e, returns true when e is contained in s
func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

//helper_function
//SetupDB returns pointer to APIMongoDB struct
func SetUpDB() *APIMongoDB {
	db := APIMongoDB{
		"mongodb://127.0.0.1:27017",
		"paragliding",
		"tracks",
	}
	return &db
}
