package main

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type APIMongoDB struct {
	Host                string
	DatabaseName        string
	TrackCollectionName string
}

func (db *APIMongoDB) Init() {
	session, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}
	defer session.Close()
}

func (db *APIMongoDB) AddTrack(t TrackInfo) error {
	session, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}

	defer session.Close()

	errInsert := session.DB(db.DatabaseName).C(db.TrackCollectionName).Insert(t)
	if errInsert != nil {
		fmt.Printf("Error in Insert(): %v", errInsert.Error())
		return errInsert
	}
	return nil
}

func (db *APIMongoDB) CountTracks() int {
	session, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}
	defer session.Close()
	count, errCount := session.DB(db.DatabaseName).C(db.TrackCollectionName).Count()
	if errCount != nil {
		fmt.Printf("Error in Count(): %v", errCount.Error())
		return -1
	}
	return count
}

func (db *APIMongoDB) GetTrackByID(id int) (TrackInfo, bool) {
	session, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}
	defer session.Close()
	track := TrackInfo{}
	foundTrack := true
	errFind := session.DB(db.DatabaseName).C(db.TrackCollectionName).Find(bson.M{"id": id}).One(&track)
	if errFind != nil {
		foundTrack = false
	}
	return track, foundTrack
}
func (db *APIMongoDB) GetTrackByURL(url string) (TrackInfo, bool) {
	session, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}
	defer session.Close()
	track := TrackInfo{}
	foundTrack := true
	errFind := session.DB(db.DatabaseName).C(db.TrackCollectionName).Find(bson.M{"url": url}).One(&track)
	if errFind != nil {
		foundTrack = false
	}
	return track, foundTrack
}

func (db *APIMongoDB) DeleteTrack(t TrackInfo) (allIsWell bool) {
	session, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}
	defer session.Close()
	allIsWell = true
	err2 := session.DB(db.DatabaseName).C(db.TrackCollectionName).Remove(t)

	if err2 != nil {
		allIsWell = false
	}
	return
}
