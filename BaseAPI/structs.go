package main

//APIInfoResponse used to format api info response
type APIInfoResponse struct {
	Duration string `json:"duration"`
	Info     string `json:"info"`
	Version  string `json:"version"`
}

//RegisterIgcStruct used to parse register url from json
type RegisterIgcStruct struct {
	URL string `json:"url"`
}

type TrackInfo struct {
	URL       string `json:"url"`
	ID        int    `json:"id"`
	TimeStamp int64  `json:"timeStamp"`
}

//IDResponse contains id for responses
type IDResponse struct {
	ID int `json:"id"`
}

//IDSliceResp contains slice of ids for response
type IDSliceResp struct {
	ID []int `json:"ids"`
}

//TrackInfoResp contains info for api call response
type TrackInfoResp struct {
	Date        string  `json:"h_date"`
	Pilot       string  `json:"pilot"`
	Glider      string  `json:"glider"`
	GliderID    string  `json:"glider_id"`
	TrackLenght float64 `json:"track_length"`
	SourceURL   string  `json:"track_src_url"`
}

type TickerResp struct {
	Latest     int64 `json:"t_latest"`
	Start      int64 `json:"t_start"`
	Stop       int64 `json:"t_stop"`
	Tracks     []int `json:"tracks"`
	Processing int64 `json:"processing"`
}
