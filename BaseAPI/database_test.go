package main

import (
	"gopkg.in/mgo.v2"
	"testing"
	"time"
)

//setting up the database for testing
func setUpDB(t *testing.T) *APIMongoDB {
	db := APIMongoDB{
		"mongodb://127.0.0.1:27017",
		"testParagliding",
		"tracks",
	}

	session, err := mgo.Dial(db.Host)
	defer session.Close()

	if err != nil {
		t.Error(err)
	}
	return &db
}

//deleting the database after testing
func tearDownDB(t *testing.T, db *APIMongoDB) {
	session, err := mgo.Dial(db.Host)
	if err != nil {
		t.Error(err)
	}

	err = session.DB(db.DatabaseName).DropDatabase()
	if err != nil {
		t.Error(err)
	}
}

func TestAPIMongoDB_AddTrack(t *testing.T) {
	db := setUpDB(t)
	defer tearDownDB(t, db)

	db.Init()
	if db.CountTracks() != 0 {
		t.Error("database not properly initiated, should be empty")
	}
	track := TrackInfo{}
	track.URL = "www.example.com"
	track.ID = db.CountTracks()
	track.TimeStamp = time.Now().Unix()
	_ = db.AddTrack(track)

	if db.CountTracks() != 1 {
		t.Error("Did not insert properly")
	}
}

func TestAPIMongoDB_GetTrack(t *testing.T) {
	db := setUpDB(t)
	defer tearDownDB(t, db)

	db.Init()
	if db.CountTracks() != 0 {
		t.Error("database not properly initiated, should be empty")
	}
	track := TrackInfo{}
	track.URL = "www.example.com"
	track.ID = db.CountTracks()
	track.TimeStamp = time.Now().Unix()
	_ = db.AddTrack(track)

	if db.CountTracks() != 1 {
		t.Error("Did not insert properly")
	}

	track2, found := db.GetTrackByID(0)

	if !found {
		t.Error("did not find track")
	}

	if track.URL != track2.URL || track.TimeStamp != track2.TimeStamp || track.ID != track2.ID {
		t.Error("Not equal tracks")
	}

}

func TestAPIMongoDB_GetTrackByURL(t *testing.T) {
	db := setUpDB(t)
	defer tearDownDB(t, db)

	db.Init()
	if db.CountTracks() != 0 {
		t.Error("database not properly initiated, should be empty")
	}
	track := TrackInfo{}
	track.URL = "www.example.com"
	track.ID = db.CountTracks()
	track.TimeStamp = time.Now().Unix()
	_ = db.AddTrack(track)

	if db.CountTracks() != 1 {
		t.Error("Did not insert properly")
	}

	track2, found := db.GetTrackByURL(track.URL)

	if !found {
		t.Error("did not find track")
	}

	if track.URL != track2.URL || track.TimeStamp != track2.TimeStamp || track.ID != track2.ID {
		t.Error("Not equal tracks")
	}

}

func TestAPIMongoDB_DeleteTrack(t *testing.T) {
	db := setUpDB(t)
	defer tearDownDB(t, db)

	db.Init()
	if db.CountTracks() != 0 {
		t.Error("database not properly initiated, should be empty")
	}
	track := TrackInfo{}
	track.URL = "www.example.com"
	track.ID = db.CountTracks()
	track.TimeStamp = time.Now().Unix()
	_ = db.AddTrack(track)

	if db.CountTracks() != 1 {
		t.Error("Did not insert properly")
	}

	db.DeleteTrack(track)

	if db.CountTracks() != 0 {
		t.Error("Did delete properly")
	}

}
