package main

import (
	"fmt"
	"net/http"
	"os"
)

//main function of program
func main() {
	Init()
	http.HandleFunc("/", NotFoundHandler)
	http.HandleFunc("/igcinfo/api/", IgcHandler)
	http.HandleFunc("/api/ticker/", TickerHandler)
	http.HandleFunc("/api/ticker/latest/", TickerLatestHandler)
	fmt.Println("Running on" + os.Getenv("PORT"))
	http.ListenAndServe(":5050" /*+os.Getenv("PORT")*/, nil)
}
