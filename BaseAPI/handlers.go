package main

import (
	"net/http"
	"strings"
)

//NotFoundHandler runs all calls that do not follow API specification
func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
}

//IgcHandler takes all calls that follow API specification
func IgcHandler(w http.ResponseWriter, r *http.Request) {
	valid := ValidURL()
	if r.URL.String() == valid[0] {
		APIInfo(w)
	} else if r.URL.String() == valid[1] {
		if URLIgcLen(r.URL.String()) && r.Method == http.MethodPost {
			RegisterIgcURL(w, r)
		} else if URLIgcLen(r.URL.String()) && r.Method == http.MethodGet {
			GetTrackInfo(w)
		}
	} else if APICallWithID(r.URL.String()) == true {
		GetGlideInfo(w, r)
	} else if APICallWithField(r.URL.String()) == true {
		GetFieldInfo(w, r)
	} else {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
	}
}

func TickerHandler(w http.ResponseWriter, r *http.Request) {
	s := DeleteEmpty(strings.Split(r.URL.String(), "/"))
	if len(s) == 3 {
		http.Error(w, http.StatusText(300), 300)
	} else if len(s) == 2 {
		Ticker(w)
	} else {

	}
}

func TickerLatestHandler(w http.ResponseWriter, r *http.Request) {
	TickerLatest(w, r)
}
